The canonical home page for this project is https://codeberg.org/aerique/emacs-theme-aerique

(The library is also pushed to GitLab and Sourcehut but those sites are not monitored for support.)

## Status: Not Maintained

I've been using [Doom Emacs](https://github.com/doomemacs/doomemacs#doom-emacs)
for a few years now and ported this theme to it, so while this specific repo is
not actively maintained the theme itself is and I will make it available at
some future date.

## Emacs Theme: aerique

Theme centered on minimal syntax highlighting and distinct but not too
harsh or distracting colors.  The only active font-lock faces are:
builtin, comment and string.

The point of this theme is not highlighting all kinds of different syntax
but partitioning the source code in easily scannable pieces.

<img src="https://codeberg.org/aerique/emacs-theme-aerique/raw/branch/master/aerique-dark-theme-1.png">
